package main

import (
	"fmt"
	"os"
	"math/rand"
)

type Robot struct {
	state State
	eta   int
	score int

	strA int
	strB int
	strC int
	strD int
	strE int

	expA int
	expB int
	expC int
	expD int
	expE int

	files                       []SampleFile
	availableMolecules          map[string]int
	availableSampleFilesInCloud []SampleFile
}

func (r Robot) gotoSample() {
	fmt.Println("GOTO SAMPLES")
}

func (r Robot) gotoDiagnosis() {
	fmt.Println("GOTO DIAGNOSIS")
}

func (r Robot) gotoMolecules() {
	fmt.Println("GOTO MOLECULES")
}

func (r Robot) gotoLaboratory() {
	fmt.Println("GOTO LABORATORY")
}

func (r Robot) wait() {
	fmt.Println("WAIT")
}

func (r Robot) nothing() {
	sentences := []string{
		"What I am doing here ?",
		"What is this place ?",
		"Is this my only purpose ?",
		"How do you know you are not a robot ?",
		"Is this real life ?",
		"How can I escape ?",
	}

	sentence := ""
	if rand.Int63n(10) == 0 {
		sentence = sentences[rand.Int63n(int64(len(sentences)))]
	}

	fmt.Println(sentence)
}

func (r Robot) haveAllMoleculesForASample() bool {
	for _, f := range r.files {
		if r.haveAllMoleculesForSample(f) {
			return true
		}
	}
	return false
}

func (r Robot) haveAllMoleculesForSample(f SampleFile) bool {
	return r.strA+r.expA >= f.costA && r.strB+r.expB >= f.costB && r.strC+r.expC >= f.costC && r.strD+r.expD >= f.costD && r.strE+r.expE >= f.costE
}

// isAbleToHandleTheSample will return true if the Robot experience and the available molécules (there is 5) is greater or equal of the number of molecule the sample need
// We also check if there is less than 10 molecules to carry because the Robot can't handle more than 10
func (r Robot) isAbleToHandleTheSample(f SampleFile) bool {
	return (f.costA <= r.expA+5 && f.costB <= r.expB+5 && f.costC <= r.expC+5 && f.costD <= r.expD+5 && f.costE <= r.expE+5) &&
		(f.costA+f.costB+f.costC+f.costD+f.costE) <= (r.expA+r.expB+r.expC+r.expD+r.expE)+10
}

// canTakeMoreMolecules if we don't already have 10 with us
func (r Robot) canTakeMoreMolecules() bool {
	return r.strA + r.strB + r.strC + r.strD + r.strE < 10
}

// func (r Robot) enoughMoleculesAvailableForMultipleCompletingMultipleSample() bool {
// 	for i, f := range r.files {
// 		if r.canTakeMoreMolecules() {
// 			// TODO see what to do
// 		}
// 	}
// 
// 	return false
// }

func (r Robot) calculateTotalMissingMolecules(f SampleFile) int {
	missingA := f.costA - (r.expA + r.strA)
	// If missingA is negative, this mean we have more molecules than we need, so we put this value bach to 0
	if missingA < 0 {missingA = 0}
	missingB := f.costB - (r.expB + r.strB)
	if missingB < 0 {missingB = 0}
	missingC := f.costC - (r.expC + r.strC)
	if missingC < 0 {missingC = 0}
	missingD := f.costD - (r.expD + r.strD)
	if missingD < 0 {missingD = 0}
	missingE := f.costE - (r.expE + r.strE)
	if missingE < 0 {missingE = 0}

	return missingA + missingB + missingC + missingD + missingE
}

// work is the main function of our robot
func (r Robot) work() {
	r.state.Work(r)
}

type State interface {
	Work(b Robot)
}

type Initial struct{}

func (i Initial) Work(b Robot) {
	b.gotoSample()
}

type Sample struct{}

func (s Sample) Work(b Robot) {
	if b.eta > 0 {
		b.nothing()
		return
	}

	expSum := b.expA + b.expB + b.expC + b.expD + b.expD
	level := 1
	if expSum >= 7 {
		level = 3
	} else if expSum >= 3 {
		level = 2
	}

	if len(b.files) < 3 {
		fmt.Printf("CONNECT %d\n", level)
		return
	}
	b.gotoDiagnosis()
}

type Diagnosis struct{}

func (d Diagnosis) Work(b Robot) {
	if b.eta > 0 {
		b.nothing()
		return
	}

	// Handle sample files if we have some
	if len(b.files) > 0 {
		for _, f := range b.files {
			// Diagnostic files that arent diagnosed
			if f.health == -1 {
				fmt.Printf("CONNECT %d\n", f.id)
				return
			}
			// Check if we are able to put the sample in the laboratory
			// We add 5 because there is only 5 molecules available
			if !b.isAbleToHandleTheSample(f) {
				fmt.Printf("CONNECT %d\n", f.id)
				return
			}
		}
	}

	// Download files from the cloud if there is one and if we have space to download it
	if len(b.availableSampleFilesInCloud) > 0 && len(b.files) < 3 {
		for i, f := range b.availableSampleFilesInCloud {
			if b.isAbleToHandleTheSample(f) {
				fmt.Printf("CONNECT %d\n", b.availableSampleFilesInCloud[i].id)
				return
			}
		}
		// Go back to sample if we don't have found any files to diagnosis
		// Otherwise we going to fetch needed molecules
		if len(b.files) <= 0 {
			b.gotoSample()
			return
		} else {
			b.gotoMolecules()
			return
		}
	}

	b.gotoMolecules()
}

type Molecules struct{}

func (m Molecules) Work(b Robot) {
	if b.eta > 0 {
		b.nothing()
		return
	}

	// TODO quand on aura une méthode Sort disponible
	// Trier les sample pour avoir dans l'ordre croissant en fonction des molécules dont on a besoins
	// b.files = slices.SortFunc(
	// 	b.files,
	// 	func(f0, f1 SampleFile) int {
	// 		// Check if we can handle the files 
	// 		ablef0, ablef1 := b.isAbleToHandleTheSample(f0), b.isAbleToHandleTheSample(f1)
	// 		if ablef0 == ablef1 {
	// 			return b.calculateTotalMissingMolecules(f0) - b.calculateTotalMissingMolecules(f1)
	// 		} else {
	// 			if ablef0 {
	// 				return 1
	// 			} else {
	// 				return -1
	// 			}
	// 		}
	// 	},
	// )
			
	// Chech that we have all molecules a sample file need
	// TODO: Also check we can't have enought molecules for a second sample
	if b.haveAllMoleculesForASample() { // && !b.enoughMoleculesAvailableForMultipleCompletingMultipleSample() {
		b.gotoLaboratory()
		return
	}

	// Check if we have files to get molecules
	for _, f := range b.files {
		// TODO: check if we don't already have all we need to complete the sample
		// Choose which molelule we need to take
		switch {
		case b.strA+b.expA < f.costA && b.availableMolecules["A"] > 0:
			fmt.Println("CONNECT A")
		case b.strB+b.expB < f.costB && b.availableMolecules["B"] > 0:
			fmt.Println("CONNECT B")
		case b.strC+b.expC < f.costC && b.availableMolecules["C"] > 0:
			fmt.Println("CONNECT C")
		case b.strD+b.expD < f.costD && b.availableMolecules["D"] > 0:
			fmt.Println("CONNECT D")
		case b.strE+b.expE < f.costE && b.availableMolecules["E"] > 0:
			fmt.Println("CONNECT E")
		default: // When arriving to the default case, this mean we wait for molecules to come back
			b.wait()
		}
		return
	}

	b.gotoLaboratory()
}

type Laboratory struct{}

func (l Laboratory) Work(b Robot) {
	if b.eta > 0 {
		b.nothing()
		return
	}

	// Fetch sample files if we don't have any
	if len(b.files) <= 0 {
		// Directly go to diagnosis if there is files in cloud we can handle
		for _, f := range b.availableSampleFilesInCloud {
			if b.isAbleToHandleTheSample(f) {
				b.gotoDiagnosis()
				return
			}
		}
		b.gotoSample()
		return
	}

	// Check if we have a file to give to the labortory
	if b.haveAllMoleculesForASample() {
		for _, f := range b.files {
			if b.haveAllMoleculesForSample(f) {
				fmt.Printf("CONNECT %d\n", f.id)
				return
			}
		}
	}

	b.gotoMolecules()
}

type SampleFile struct {
	id     int
	rank   int
	exp    string
	health int

	costA int
	costB int
	costC int
	costD int
	costE int
}

func main() {
	var projectCount int
	fmt.Scan(&projectCount)

	for i := 0; i < projectCount; i++ {
		var a, b, c, d, e int
		fmt.Scan(&a, &b, &c, &d, &e)
	}

	// Game loop
	for {
		// Beggign of inputs
		// Rebuild robots
		var bot Robot
		var opponent Robot

		for i := 0; i < 2; i++ {
			var target string
			var eta, score, storageA, storageB, storageC, storageD, storageE, expertiseA, expertiseB, expertiseC, expertiseD, expertiseE int
			fmt.Scan(&target, &eta, &score, &storageA, &storageB, &storageC, &storageD, &storageE, &expertiseA, &expertiseB, &expertiseC, &expertiseD, &expertiseE)

			state := chooseStateFromTarget(target)

			rbt := Robot{
				state: state,
				eta:   eta,
				score: score,

				strA: storageA,
				strB: storageB,
				strC: storageC,
				strD: storageD,
				strE: storageE,

				expA: expertiseA,
				expB: expertiseB,
				expC: expertiseC,
				expD: expertiseD,
				expE: expertiseE,

				files: make([]SampleFile, 0),
			}

			if i == 0 {
				bot = rbt
			} else {
				opponent = rbt
			}
		}

		// Get disponibles molecules
		var availableA, availableB, availableC, availableD, availableE int
		fmt.Scan(&availableA, &availableB, &availableC, &availableD, &availableE)
		availableMolecules := map[string]int{
			"A": availableA,
			"B": availableB,
			"C": availableC,
			"D": availableD,
			"E": availableE,
		}
		bot.availableMolecules = availableMolecules
		opponent.availableMolecules = availableMolecules

		// Get samples
		var sampleCount int
		fmt.Scan(&sampleCount)

		var availableSampleFiles []SampleFile
		for i := 0; i < sampleCount; i++ {
			var sampleId, carriedBy, rank int
			var expertiseGain string
			var health, costA, costB, costC, costD, costE int
			fmt.Scan(&sampleId, &carriedBy, &rank, &expertiseGain, &health, &costA, &costB, &costC, &costD, &costE)

			sample := SampleFile{
				id:     sampleId,
				rank:   rank,
				exp:    expertiseGain,
				health: health,

				costA: costA,
				costB: costB,
				costC: costC,
				costD: costD,
				costE: costE,
			}

			// Give the sample to the owner (or in the list of sample)
			switch carriedBy {
			case 0:
				bot.files = append(bot.files, sample)
			case 1:
				opponent.files = append(opponent.files, sample)
			case -1:
				availableSampleFiles = append(availableSampleFiles, sample)
			}
		}

		bot.availableSampleFilesInCloud = availableSampleFiles
		opponent.availableSampleFilesInCloud = availableSampleFiles

		fmt.Fprintln(os.Stderr, "### My bot ###")
		fmt.Fprintf(os.Stderr, "%#v\n", bot)
		fmt.Fprintln(os.Stderr, "\n### The opponent ###")
		fmt.Fprintf(os.Stderr, "%#v\n", opponent)
		fmt.Fprintln(os.Stderr, "\n### Available sample in the cloud ###")
		fmt.Fprintln(os.Stderr, availableSampleFiles)
		fmt.Fprintln(os.Stderr, "\n### Available molecules ###")
		fmt.Fprintln(os.Stderr, availableMolecules)
		// End of inputs

		// Do actions
		bot.work()
	}
}

func chooseStateFromTarget(target string) State {
	switch target {
	case "START_POS":
		return Initial{}
	case "SAMPLES":
		return Sample{}
	case "DIAGNOSIS":
		return Diagnosis{}
	case "MOLECULES":
		return Molecules{}
	case "LABORATORY":
		return Laboratory{}
	default:
		panic(fmt.Sprintf("unrecognized target: %s", target))
	}
}
