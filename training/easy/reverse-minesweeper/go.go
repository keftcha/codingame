package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Buffer(make([]byte, 1000000), 1000000)

	var w int
	scanner.Scan()
	fmt.Sscan(scanner.Text(), &w)

	var h int
	scanner.Scan()
	fmt.Sscan(scanner.Text(), &h)

	lines := make([]string, h)
	for i := 0; i < h; i++ {
		scanner.Scan()
		line := scanner.Text()
		lines[i] = line
	}

	grid := initializeEmptyGrid(w, h)

	for lineIdx := range lines {
		for columnIdx := range lines[lineIdx] {
			if lines[lineIdx][columnIdx] == 'x' {
				incrementAround(columnIdx, lineIdx, grid)
			}
		}
	}

	displayGrid(grid)
}

func initializeEmptyGrid(columns, lines int) [][]int {
	grid := make([][]int, lines)
	for i := 0; i < lines; i++ {
		line := make([]int, columns)
		grid[i] = line
	}
	return grid
}

func incrementAround(x, y int, grid [][]int) {
	grid[y][x] = -1

	for lineIdx := -1; lineIdx < 2; lineIdx++ {
		for columnIdx := -1; columnIdx < 2; columnIdx++ {
			if y+lineIdx >= 0 && y+lineIdx < len(grid) && x+columnIdx >= 0 && x+columnIdx < len(grid[0]) && grid[y+lineIdx][x+columnIdx] != -1 {
				grid[y+lineIdx][x+columnIdx]++
			}
		}
	}
}

func displayGrid(grid [][]int) {
	for _, line := range grid {
		for _, column := range line {
			switch column {
			case -1, 0:
				fmt.Print(".")
			case 1, 2, 3, 4, 5, 6, 7, 8:
				fmt.Print(strconv.Itoa(column))
			default:
				panic(fmt.Sprintf("non handeled value: %d", column))
			}
		}
		fmt.Println()
	}
}
